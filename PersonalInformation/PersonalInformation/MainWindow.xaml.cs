﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PersonalInformation
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        VM vm = new VM();
        PersonalInformationWindow pi;
        FriendWindow fw;
        FamilyWindow fm;
        public MainWindow()
        {
            InitializeComponent();
            DataContext = vm;
        }

        private void BtnSurriya_Click(object sender, RoutedEventArgs e)
        //{
        //    vm.SurriyaWindow();
        //}
        {
            if (pi == null)
            {
                pi = new PersonalInformationWindow
                {
                   
                };
                //sw = new SecondWindow(++count);
                pi.Closed += Pi_Closed;
                pi.Show();
                pi.PersonalInformationWindowForm();
            }
        }

        private void Pi_Closed(object sender, EventArgs e)
        {
            pi = null;
        }

        private void BtnFriend_Click(object sender, RoutedEventArgs e)
        {
            if (fw == null)
            {
                fw = new FriendWindow
                {

                };
                //sw = new SecondWindow(++count);
                fw.Closed += Fw_Closed;
                fw.Show();
                fw.FriendWindowForm();
            }
        }

        private void Fw_Closed(object sender, EventArgs e)
        {
            fw = null;
        }

        private void BtnFamily_Click(object sender, RoutedEventArgs e)
        {
            if (fm == null)
            {
                fm = new FamilyWindow
                {

                };
                //sw = new SecondWindow(++count);
                fm.Closed += Fm_Closed;                
                fm.FamilyWindowForm();
                
                fm.Show();
            }
        }

        private void Fm_Closed(object sender, EventArgs e)
        {
            fm = null;
        }


        private void BtnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (pi != null)
                pi.Close();
            if (fw != null)
                fw.Close();
            if (fm != null)
                fm.Close();
        }
    }
}
