﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PersonalInformation
{
    /// <summary>
    /// Interaction logic for FriendWindow.xaml
    /// </summary>
    public partial class FriendWindow : Window
    {
        VM vm = new VM();
        public FriendWindow()
        {
            InitializeComponent();
            DataContext = vm;
        }
        public void FriendWindowForm()
        {
            Form tw = new Form() { Address = "Friend Country", Age = 100, Phone = "11111111111" };
            //Forms.Add(sw);
            vm.Address = tw.Address;
            vm.Age = tw.Age;
            vm.PhoneNumber = tw.Phone;
        }
        //Input value is transferring into vm property name 
        private void BtnCalc_Click(object sender, RoutedEventArgs e)
        {
            Form tw = new Form() { Name = vm.FriendName };
            vm.Name = tw.Name;

                    //OR

            //vm.Name = vm.FriendName;

        }
    }
}
