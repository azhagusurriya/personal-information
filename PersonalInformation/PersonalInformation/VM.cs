﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace PersonalInformation
{
    class VM : INotifyPropertyChanged
    {
        private string name;
        public string Name { get { return name; } set { name = value; OnChange(); } }
        private string address;
        public string Address { get { return address; } set { address = value; OnChange(); } }
        private int age;
        public int Age { get { return age; } set { age = value; OnChange(); } }
        private string phoneNumber;
        public string PhoneNumber { get { return phoneNumber; } set { phoneNumber = value; OnChange(); } }

        private string friendName;
        public string FriendName { get { return friendName; } set { friendName = value; OnChange(); } }

        public BindingList<Form> Forms { get; set; } = new BindingList<Form>();

        public Form form = new Form();


        #region PropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnChange([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

    }
}
