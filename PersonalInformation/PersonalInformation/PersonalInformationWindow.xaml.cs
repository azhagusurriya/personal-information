﻿using System.Windows;

namespace PersonalInformation
{
    /// <summary>
    /// Interaction logic for PersonalInformationWindow.xaml
    /// </summary>
    public partial class PersonalInformationWindow : Window
    {
        VM vm = new VM();
        public PersonalInformationWindow()
        {
            InitializeComponent();
            DataContext = vm;

        }
        public void PersonalInformationWindowForm()
        {
            Form sw = new Form() { Name = "Surriya", Address = "India", Age = 24, Phone = "2049799296" };
            //Forms.Add(sw);
            vm.Forms.Add(sw);
            vm.Name = sw.Name;
            vm.Address = sw.Address;
            vm.Age = sw.Age;
            vm.PhoneNumber = sw.Phone;
        }
    }
}
