﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PersonalInformation
{
    /// <summary>
    /// Interaction logic for FamilyWindow.xaml
    /// </summary>
    public partial class FamilyWindow : Window
    {
        VM vm = new VM();
        public FamilyWindow()
        {
            InitializeComponent();
            DataContext = vm;
        }
        public static readonly char[] DELIMITER = new char[] { ',', ' ', '.' };
        public void FamilyWindowForm()
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.DefaultExt = ".txt";
            fileDialog.Filter = "Text documents (.txt)|*.txt";
            if (fileDialog.ShowDialog() == true)
            {
                string[] lines = File.ReadAllLines(fileDialog.FileName);
                foreach (string line in lines)
                {
                    string[] items = line.Split(DELIMITER, StringSplitOptions.RemoveEmptyEntries);                   
                    Form fw = new Form()
                    {
                    Name = items[0],
                    Address = items[1],
                    Age = int.Parse(items[2]),
                    Phone = items[3]
                    };
                    vm.Forms.Add(fw);
                    vm.Name = items[0];
                    vm.Address = items[1];
                    vm.Age = int.Parse(items[2]);
                    vm.PhoneNumber = items[3];
                }
            }
        }
    }
}
