﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalInformation
{
    public class Form
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public int Age { get; set; }
        public string  Phone { get; set; }
    }
}
